// Reflection.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

/////////////////
///   Types   ///

/** Defined in 'Reflection/ResourceInfo.h */
class ResourceInfo;

/** Defined in 'Reflection/AssetInfo.h' */
class AssetInfo;
