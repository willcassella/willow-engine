// Resource.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

/////////////////
///   Types   ///

/** Defined in 'Resource.h' */
class Resource;

/** Defined in 'Asset.h' */
class Asset;

/** Defined in 'ResourceManager.h' */
struct ResourceManager;

/** Defined in 'AssetManager.h' */
struct AssetManager;

/** Defined in 'AssetPtr.h' */
template <class AssetT>
struct AssetPtr;
