// Core.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

/////////////////
///   Types   ///

/** Defined in 'String.h' */
struct String;

/** Defined in 'Object.h */
class Object;

/** Defined in 'Interface.h' */
class Interface;

/** Defined in 'Application.h' */
struct Application;

/** Defined in 'ArchNode.h' */
class ArchNode;
