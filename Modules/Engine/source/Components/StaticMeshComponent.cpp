// StaticMeshComponent.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include "../../include/Engine/GameObject.h"
#include "../../include/Engine/Scene.h"
#include "../../include/Engine/Components/StaticMeshComponent.h"

//////////////////////
///   Reflection   ///

BUILD_REFLECTION(StaticMeshComponent);
